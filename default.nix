{ mkDerivation
, base
, containers
, mtl
, hspec
, optparse-applicative
, stdenv }:
let basicDeps = [ base containers mtl ]; in
mkDerivation {
  pname = "lift";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = basicDeps ++ [  ];
  executableHaskellDepends = basicDeps ++ [ optparse-applicative ];
  testHaskellDepends = basicDeps ++ [ hspec ];
  testTarget = "spec";
  doHaddock = false;
  license = "unknown";
  hydraPlatforms = with stdenv.lib.platforms; darwin ++ linux;
}
