{ nixpkgs ? import <nixpkgs> {}, compiler ? "default", doBenchmark ? false }:

let

  inherit (nixpkgs) pkgs;

  f = { mkDerivation
      , base
      , containers
      , mtl
      , hspec
      , optparse-applicative
      , stdenv
      }:
        let basicDeps = [ base containers mtl ]; in
      mkDerivation {
        pname = "lift";
        version = "0.1.0.0";
        src = ./.;
        isLibrary = true;
        isExecutable = true;
        libraryHaskellDepends = basicDeps ++ [ ];
        executableHaskellDepends = basicDeps ++ [ optparse-applicative ];
        testHaskellDepends = basicDeps ++ [ hspec ];
        doHaddock = true;
        license = "GPLv3";
        hydraPlatforms = with pkgs.stdenv.lib.platforms; darwin ++ linux;
      };

  haskellPackages = if compiler == "default"
                       then pkgs.haskellPackages
                       else pkgs.haskell.packages.${compiler};

  variant = if doBenchmark then pkgs.haskell.lib.doBenchmark else pkgs.lib.id;

  drv = variant (haskellPackages.callPackage f {});

in

if pkgs.lib.inNixShell
  then drv.envFunc { withHoogle = true; }
  else drv
