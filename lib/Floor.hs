-- | Floor information for cli and pass to scheduler.

module Floor where

type FloorName = [String]

floorCount :: FloorName -> Int
floorCount = length
