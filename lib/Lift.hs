-- | Lift module contains lift data struct and its State transformation. They
-- are called by scheduler.

module Lift
  ( LiftFloor
  , LiftState(..)
  , Lift(..)
  , LiftMoveT(..)
  , caseMoveT
  , (-:-)
  , (-:)
  , moveLUp, moveLDown
  , idleS, loadS, exceptS, moveSUp, moveSDown, stopS
  , runState, evalState, execState, mapState, withState
  , initLift
  ) where

import Control.Monad.State

import Floor

-- * Data struct

-- | 'LiftFloor' must be bound by 'Num'.
type LiftFloor = Int

data LiftState = LiftIdle
               | LiftMove LiftMoveT
               | LiftLoad
               | LiftException
               | LiftStop
               deriving (Eq, Show, Read)

data LiftMoveT = LiftMoveUp | LiftMoveDown
    deriving (Eq, Show, Read)

caseMoveT :: f           -- ^ return if Up
          -> f           -- ^ return if Down
          -> LiftMoveT
          -> f
caseMoveT a _ LiftMoveUp   = a
caseMoveT _ b LiftMoveDown = b

data Lift = Lift { curFloor   :: LiftFloor
                 , curState   :: LiftState
                 , floorNames :: FloorName
                 }
            deriving (Eq, Show, Read)

initLift :: Lift
initLift = Lift  0 LiftIdle []

-- | Calculate the distance by a lift state and a floor.
-- Negative means the floor is different of the movement direction
(-:) :: Lift -> LiftFloor -> Int
(Lift floor (LiftMove direction) _) -: floor' =
    caseMoveT up down direction
  where
    up   = floor' - floor
    down = floor - floor'
(Lift floor _ _) -: floor' = abs $ floor - floor'

-- | Calculate the distance by a lift state and a floor. But it convert the
-- negative to the floor count plus its abs.
-- e.g. -1 -> count + 1
--
(-:-) :: Lift -> LiftFloor -> Int
fl@(Lift _ _ names) -:- fl' = convert $ fl -: fl'
  where
    convert x
        | x < 0     = abs x + floorCount names
        | otherwise = x

-- * State transformation

-- | move lift floor up, this state transformation don't change 'Lift.curState'
moveLUp :: State Lift LiftFloor
moveLUp = do
    s@(Lift fl _ names) <- get
    let fl' = if fl < floorCount names
                then fl + 1
                else fl
    put $ s { curFloor = fl' }
    return fl'

-- | move lift floor down, this state transformation don't change 'Lift.curState'
moveLDown :: State Lift LiftFloor
moveLDown = do
    s@(Lift fl _ _) <- get
    let fl' = if fl > 0
                then fl - 1
                else fl
    put $ s { curFloor = fl' }
    return fl'

idleS :: State Lift LiftFloor
idleS = do
    s <- get
    put $ s { curState = LiftIdle }
    return $ curFloor s

loadS :: State Lift LiftFloor
loadS = do
    s <- get
    put $ s { curState = LiftLoad }
    return $ curFloor s

exceptS :: State Lift LiftFloor
exceptS = do
    s <- get
    put $ s { curState = LiftException }
    return $ curFloor s

stopS :: State Lift LiftFloor
stopS = do
    s <- get
    put $ s { curState = LiftStop }
    return $ curFloor s

-- | move lift state to up, this state transformation do change 'curState'.
moveSUp :: State Lift LiftFloor
moveSUp = do
  modify $ \s -> s { curState = LiftMove LiftMoveUp }
  moveLUp

-- | move lift state to down, this state transformation do change 'curState'.
moveSDown :: State Lift LiftFloor
moveSDown = do
  modify $ \s -> s { curState = LiftMove LiftMoveDown }
  moveLDown
