-- | A queue for task.

module Scheduler.TaskQueue
  ( TaskQueue
  , popTask
  , pushTask
  , sortTask
  , taskCount
  , emptyTask
  , module Data.Sequence
  ) where

import Data.Sequence
import Control.Exception
import Lift

-- | 'TaskQueue' should satisficate @Eq a => Seq a@.
type TaskQueue = Seq LiftFloor

-- | Pop the next task by a task queue and current lift state.
popTask :: TaskQueue                -- ^ current task queue
        -> Lift                     -- ^ current lift state
        -> (LiftFloor, TaskQueue)   -- ^ @(next-floor, rest-queue)@
popTask t c = pop $ sortOn (sortTask c) t
  where
    pop Empty = (curFloor c, t)
    pop (x :<| xs) = (x, xs)

-- | Push a new task into the queue
pushTask :: TaskQueue -> LiftFloor -> TaskQueue
pushTask = (|>)

sortTask :: Lift
         -> LiftFloor
         -> Int         -- ^ distance
sortTask = (-:-)

taskCount = Data.Sequence.length

emptyTask :: TaskQueue
emptyTask = Empty
