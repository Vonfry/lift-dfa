-- | A scheduler is to get argument and call lift program to make it working.
-- This module contains a task queue and control unit, but the IO loop isn't
-- managed by this module, which should be done in IO module. And IO module
-- calls this module everytime neccessary.

module Scheduler where

import Lift
import Scheduler.TaskQueue
import Data.List
import Data.Maybe

-- | Add the floor into task queue
onFloor :: String -> Lift -> TaskQueue -> TaskQueue
onFloor s l q =  pushTask q $ s <! l

-- | do next task
doTask :: LiftFloor
       -> Lift
       -> Either Lift (LiftFloor, Lift)  -- ^ If next task is the same, it is
                                         -- 'Left', otherwise it is 'Right'. And
                                         -- the LiftFloor is the same as input.
doTask fl' l@(Lift fl _ _)
    | fl' == fl = Left $ doIdle l
    | fl' <  fl = Right (fl', doFloorDown l)
    | fl' >  fl = Right (fl', doFloorUp   l)

doFloorUp :: Lift -> Lift
doFloorUp = execState moveSUp

doFloorDown :: Lift -> Lift
doFloorDown = execState moveSDown

-- | open the lift door
onOpen :: Lift -> Lift
onOpen = doLoad

-- | close the lift door
onClose :: Lift -> Lift
onClose = doIdle

-- | idle
doIdle :: Lift -> Lift
doIdle = execState idleS

-- | load
doLoad :: Lift -> Lift
doLoad = execState loadS

-- | something error
onExcept :: Lift -> Lift
onExcept = doExcept

-- | something error
doExcept :: Lift -> Lift
doExcept = execState exceptS

-- | stop lift
doStop :: Lift -> Lift
doStop = execState stopS

-- | input a floorname and returnn its mapping for 'LiftFloor'.
(<!) :: String -> Lift -> LiftFloor
(<!) s (Lift _ _ names) = fromJust $ elemIndex s names
