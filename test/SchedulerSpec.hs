module SchedulerSpec where

import Test.Hspec
import Test.Hspec.QuickCheck
import Scheduler
import Scheduler.TaskQueue
import Lift
import Floor
import Data.Function
import Data.Either

spec :: Spec
spec = describe "Scheduler" $ do
    it "on move up" $
        (==liftMoveUp) moveUpL `shouldBe` True
    it "on move down" $
        (==liftMoveDown) moveDownL `shouldBe` True
    it "on open" $
        (==liftLoad) openL `shouldBe` True
    it "on close" $
        (==liftIdle)  closeL `shouldBe` True
    it "on except" $
        (==liftException) exceptL `shouldBe` True
    prop "on floor" $
        \x -> onFloor (floorNames lift !! modX x) lift task == task :|> (modX x)
    prop "task" $ \x -> ((modX x)==) $ curFloor $
        fromLeft (lift { curFloor = -1 }) $
        (iterate ((=<<) $ uncurry doTask) (Right (modX x, lift))) !! count
  where
    lift :: Lift
    lift  = Lift 3 LiftIdle ["B1","F0","F1","F2","F3","F4"]

    count = taskCount task

    task = 4 :<| 5 :<| 3 :<| Empty

    moveUpL   = doFloorUp   lift
    moveDownL = doFloorDown lift
    exceptL   = onExcept    lift
    openL     = onOpen      lift
    closeL    = onClose     lift

    liftMoveUp = lift { curFloor = 4
                      , curState = LiftMove LiftMoveUp
                      }
    liftMoveDown = lift { curFloor = 2
                        , curState = LiftMove LiftMoveDown
                        }
    liftIdle = lift {curState = LiftIdle }
    liftLoad = lift {curState = LiftLoad }
    liftException = lift {curState = LiftException }

    modX x = if x `mod` count == 0
             then (abs x + 1) `mod` count
             else abs x `mod` count
