module Scheduler.TaskQueueSpec where

import Test.Hspec
import Test.Hspec.QuickCheck
import Lift
import Scheduler.TaskQueue
import Floor

spec :: Spec
spec = describe "Task Queue" $ do
    prop "sort task" $
        \x -> let m = if x `mod` taskLen == 0
                      then (abs x + 1) `mod` taskLen
                      else abs x `mod` taskLen
              in sortTask current (sorted `index` (m - 1))
              < sortTask current (sorted `index` m)
    prop "push task" $
        \x -> last (pushTask task (x `mod` count)) == x `mod` count
    it "pop task" $
        fst (popTask task current) `shouldBe` (sorted `index` 0)
  where
    current :: Lift
    current  = Lift 2 (LiftMove LiftMoveUp) ["B1", "F0", "F1", "F2", "F3"]
    task    :: TaskQueue
    task     = 4 :<| 3 :<| 5 :<| 0 :<| Empty
    taskLen  = taskCount task
    count   :: Int
    count    = floorCount $ floorNames current
    last (xs :|> x) = x
    sorted = sortOn (sortTask current) task
