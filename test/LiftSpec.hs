module LiftSpec where

import Test.Hspec
import Lift

spec :: Spec
spec = describe "Lift" $ do
    context "move up" $ do
        it "fst" $ fst moveUpL `shouldBe` 4
        it "snd" $ ((==liftMoveUp) . snd) moveUpL `shouldBe` True
    context "move down" $ do
        it "fst" $ fst moveDownL `shouldBe` 2
        it "snd" $ ((==liftMoveDown) . snd) moveDownL `shouldBe` True
    context "idle" $ do
        it "fst" $ fst idleL `shouldBe` 3
        it "snd" $ ((==liftIdle) . snd) idleL `shouldBe` True
    context "load" $ do
        it "fst" $ fst loadL `shouldBe` 3
        it "snd" $ ((==liftLoad) .  snd) loadL `shouldBe` True
    context "except" $ do
        it "fst" $ fst exceptL `shouldBe` 3
        it "snd" $ ((==liftException) . snd) exceptL `shouldBe` True

  where
    lift :: Lift
    lift  = Lift 3 LiftIdle ["B1","F0","F1","F2","F3","F4"]

    moveUpL   = runState moveSUp   lift
    moveDownL = runState moveSDown lift
    exceptL   = runState exceptS   lift
    idleL     = runState idleS     lift
    loadL     = runState loadS     lift

    liftMoveUp = lift { curFloor = 4
                      , curState = LiftMove LiftMoveUp
                      }
    liftMoveDown = lift { curFloor = 2
                        , curState = LiftMove LiftMoveDown
                        }
    liftIdle = lift {curState = LiftIdle }
    liftLoad = lift {curState = LiftLoad }
    liftException = lift {curState = LiftException }
