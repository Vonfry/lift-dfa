{-# LANGUAGE MultiWayIf #-}

module Main where

import Floor
import Scheduler
import Scheduler.TaskQueue
import Lift
import Control.Concurrent (threadDelay)

import Data.List ( elem
                 , dropWhileEnd
                 , subsequences
                 )
import Data.Char ( isSpace
                 , toLower
                 )
import Options.Applicative

newtype Arguments = Arguments
  { floorsName :: String -- ^ a list of string for floors name, which are splited
                         -- by "," or spaces . Its count is used to create state mathine
  }

args :: Parser Arguments
args = Arguments
    <$> strOption
        ( showDefault
       <> value "B1,F0,F1,F2,F3,F4"
       <> metavar "LIST"
       <> help "a list of strting for floors name, which are splited by , or space.")


main :: IO ()
main = mainDefault =<< execParser opts
  where
    opts = info (args <**> helper)
      ( fullDesc
     <> progDesc "Lift simulator by DFA for my homework."
     <> header "Lift simulator." )

mainDefault :: Arguments -> IO ()
mainDefault args = do
    putStrLn $ "Your floors: " ++ show floors
    mainLoop lift
  where
    floors :: FloorName
    floors = parseFloor $ floorsName args
    parseFloor = map trim . splitOneOf ", "
    lift = initLift { floorNames = floors }

mainLoop :: Lift -> IO ()
mainLoop lift = loop $ Right (lift, emptyTask)
  where
    loop :: Either String (Lift, TaskQueue) -> IO ()
    loop (Right (lift, tasks)) = do
        putStrLn "\n------"
        tasksIn <- getTask
        parseTaskin tasksIn lift tasks
    loop (Left msg) = putStrLn msg

    -- | This subfunction is the main state machine.
    -- This struct is not good. We should move this part into 'M.Lift'.
    -- And code a parse convert input string to special data for state
    -- machine input
    parseTaskin :: [String] -> Lift -> TaskQueue -> IO ()
    parseTaskin ["open"]   lift@(Lift _ (LiftMove _) _) tasks =
        parseTaskin ["_"] lift tasks
    parseTaskin ["close"]  lift@(Lift _ (LiftMove _) _) tasks =
        parseTaskin ["_"] lift tasks
    parseTaskin ["open"]  lift tasks = do
        putStrLn "Open"
        let lift' = onOpen lift
        threadDelay delayTime
        parseTaskin ["close"] lift' tasks
    parseTaskin ["close"] lift tasks  = do
        putStrLn "Close"
        let lift' = onClose lift
        threadDelay delayTime
        loop $ Right (lift', tasks)
    parseTaskin ["stop"] lift tasks = do
        let lift' = doStop lift
        loop $ Left "Stop"
    parseTaskin ["exception"] lift tasks = do
        let lift' = doExcept lift
        loop $ Left "Exception"
    parseTaskin ["_"] lift t@Empty = do
        putStrLn "Finish"
        parseTaskin ["stop"] lift t
    parseTaskin ["_"] lift tasks = do
        let (task, tasks') = popTask tasks lift
        putStrLn "do task"
        putStrLn $ "Current floor: " ++ show (floorNames lift !! curFloor lift)
        putStrLn $ "MoveTo: " ++ show (floorNames lift !! task)
        case doTask task lift of
            Left lift' -> parseTaskin ["open"] lift' tasks'
            Right (fl', lift') -> do
                threadDelay delayTime
                loop $ Right (lift', pushTask tasks' fl')
    parseTaskin taskin lift tasks =
        let tasks' = foldr (flip onFloor lift) tasks taskin
        in parseTaskin ["_"] lift tasks'

    delayTime = 5 * 10 ^ 5

getTask :: IO [String]
getTask = do
    putStrLn "Input tasks(floor name/(c)lose/(o)pen//(s)top/(e)xception)/[_]:"
    t <- getLine
    if | elem t ["_", ""]   -> return ["_"]
       | isT t "close"      -> return ["close"]
       | isT t "open"       -> return ["open"]
       | isT t "stop"       -> return ["stop"]
       | isT t "exception"  -> return ["exception"]
       | otherwise    -> return $ parseTasks t
  where
    parseTasks :: String -> [String]
    parseTasks = map trim . splitOneOf ", "
    isT :: String -> String -> Bool
    isT t = elem (toLower <$> t) . subsequences

-- * help function

splitOneOf s =
    foldr (\a b'@(b:bs) ->
            if a `elem` s
                then "":b'
                else (a : b) : bs
        ) [""]

trim = dropWhileEnd isSpace . dropWhile isSpace
